"""Bear sensors server."""
import datetime
import json

import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web

from Adafruit_BMP085 import BMP085
from mpu6050 import MPU6050

bmp = BMP085(0x77)
mpu = MPU6050()


class WSHandler(tornado.websocket.WebSocketHandler):

    """Websocket handler."""

    def send_data(self):
        """Send data."""
        try:
            self.write_message(json.dumps({
                'temperature': bmp.readTemperature(),
                'pressure': bmp.readPressure(),
                'gyro': mpu.get_gyro(),
                'accel': mpu.get_accel(),
            }))

            tornado.ioloop.IOLoop.instance().add_timeout(
                datetime.timedelta(milliseconds=20), self.send_data
            )
        except tornado.websocket.WebSocketClosedError:
            self.on_close()

    def open(self):
        """New connection."""
        print('Client connected')
        self.send_data()

    def on_message(self, message):
        """Incoming message."""
        self.write_message('Incoming data: ' + message)

    def on_close(self):
        """Connection closed."""
        print('Client disconnected')


application = tornado.web.Application([
    (r'/()', tornado.web.StaticFileHandler, {'path': 'index.html'}),
    (r'/ws', WSHandler),
])


if __name__ == "__main__":
    mpu.initialize()
    mpu.gyro_offs = {'x': 0, 'y': 0, 'z': 0}
    mpu.accel_offs = {'y': 0, 'x': 0, 'z': 0}

    http_server = tornado.httpserver.HTTPServer(application)
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
